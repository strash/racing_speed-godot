extends Control


signal ingame_menu_close_button_pressed
signal ingame_menu_replay_button_pressed


func _on_BtnClose_pressed() -> void:
	emit_signal("ingame_menu_close_button_pressed")


func _on_BtnContinue_pressed() -> void:
	emit_signal("ingame_menu_close_button_pressed")


func _on_BtnReplay_pressed() -> void:
	emit_signal("ingame_menu_replay_button_pressed")
