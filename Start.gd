extends Control


signal start_button_pressed
signal music_setting_changed


onready var MyMenu:VBoxContainer = get_node("MainMenu")
onready var MySettings:VBoxContainer = get_node("Settings")
onready var MyLogo:TextureRect = get_node("Logo")
onready var MyCar:TextureRect = get_node("Car")
onready var MyTween:Tween = get_node("Tween")
onready var MySwitch:TextureRect = get_node("Settings/Music/Switch")
onready var MySwitchKnob:TextureRect = get_node("Settings/Music/Switch/Knob")


var logo_pos:PoolRealArray = PoolRealArray([52.0, -68.0])
var car_pos:PoolVector2Array = PoolVector2Array([Vector2(825.0, 120.0), Vector2(460.0, 185.0)])
var main_menu_pos:PoolRealArray = PoolRealArray([145.5, 425.0])
var settings_pos:PoolRealArray = PoolRealArray([425.0, 106.5])
var toggle_pos:PoolRealArray = PoolRealArray([24.5, -13.5])
var toggle_color:PoolColorArray = PoolColorArray([Color8(255, 255, 255, 255), Color8(145, 145, 145, 255)])

var is_music_on = true


# - - - - BUILTINS - - - -


func _ready() -> void:
	var _i = MyTween.interpolate_property(MyCar, "rect_position", car_pos[0], car_pos[1], 0.7, MyTween.TRANS_CUBIC, MyTween.EASE_OUT, 0.3)
	var _s = MyTween.start()


# - - - - METHODS - - - -


func toggle_switch(status: bool) -> void:
	if status:
		var _i = MyTween.interpolate_property(MySwitchKnob, "rect_position:x", toggle_pos[1], toggle_pos[0], 0.2)
		var _i_sc = MyTween.interpolate_property(MySwitch, "self_modulate", toggle_color[1], toggle_color[0], 0.2)
		var _i_skc = MyTween.interpolate_property(MySwitchKnob, "self_modulate", toggle_color[1], toggle_color[0], 0.2)
	else:
		var _i = MyTween.interpolate_property(MySwitchKnob, "rect_position:x", toggle_pos[0], toggle_pos[1], 0.2)
		var _i_sc = MyTween.interpolate_property(MySwitch, "self_modulate", toggle_color[0], toggle_color[1], 0.2)
		var _i_skc = MyTween.interpolate_property(MySwitchKnob, "self_modulate", toggle_color[0], toggle_color[1], 0.2)
	if not MyTween.is_active():
		var _s = MyTween.start()


# - - - - SIGNALS - - - -


func _on_BtnStart_pressed() -> void:
	emit_signal("start_button_pressed")


func _on_BtnSettings_pressed() -> void:
	var _i_l = MyTween.interpolate_property(MyLogo, "rect_position:y", logo_pos[0], logo_pos[1], 0.4, MyTween.TRANS_CUBIC, MyTween.EASE_OUT)
	var _i_c = MyTween.interpolate_property(MyCar, "rect_position", car_pos[1], car_pos[0], 0.4, MyTween.TRANS_CUBIC, MyTween.EASE_OUT)
	var _i_m = MyTween.interpolate_property(MyMenu, "rect_position:y", main_menu_pos[0], main_menu_pos[1], 0.4, MyTween.TRANS_CUBIC, MyTween.EASE_OUT)
	var _i_s = MyTween.interpolate_property(MySettings, "rect_position:y", settings_pos[0], settings_pos[1], 0.4, MyTween.TRANS_CUBIC, MyTween.EASE_OUT)
	if not MyTween.is_active():
		var _s = MyTween.start()


func _on_BtnBack_pressed() -> void:
	var _i_l = MyTween.interpolate_property(MyLogo, "rect_position:y", logo_pos[1], logo_pos[0], 0.4, MyTween.TRANS_CUBIC, MyTween.EASE_OUT)
	var _i_c = MyTween.interpolate_property(MyCar, "rect_position", car_pos[0], car_pos[1], 0.4, MyTween.TRANS_CUBIC, MyTween.EASE_OUT)
	var _i_m = MyTween.interpolate_property(MyMenu, "rect_position:y", main_menu_pos[1], main_menu_pos[0], 0.4, MyTween.TRANS_CUBIC, MyTween.EASE_OUT)
	var _i_s = MyTween.interpolate_property(MySettings, "rect_position:y", settings_pos[1], settings_pos[0], 0.4, MyTween.TRANS_CUBIC, MyTween.EASE_OUT)
	if not MyTween.is_active():
		var _s = MyTween.start()


func _on_Music_pressed() -> void:
	is_music_on = not is_music_on
	toggle_switch(is_music_on)
	emit_signal("music_setting_changed", is_music_on)
