extends Control


signal increase_score


export (PackedScene) var Line
export (PackedScene) var Item


onready var MyCar: Control = get_node("Car")
onready var MyTween: Tween = get_node("Tween")
onready var MyLines: Control = get_node("LineSpawn")
onready var MyItemSpawn: Control = get_node("ItemSpawn")
onready var AudioTurbo: AudioStreamPlayer = get_node("AudioTurbo")
onready var AudioNitro: AudioStreamPlayer = get_node("AudioNitro")


enum { LEFT, RIGHT } # направление
enum { NITRO, TURBO } # тип айтема

var textures = [
	preload("res://assets/image/ic_nitro.png"),
	preload("res://assets/image/ic_turbo.png"),
]

var car_pos: PoolRealArray = PoolRealArray([201.0, 461.0])
var car_dir: int = -1
var speed: float = 50.0 # скорость игры

var nitro_state: bool= false # состояние нитры
var nitro_up: float = 1.8 # коэффициент ускорения

var cost = {
	turbo = 5, # стоимость турбо
	nitro = 100, # стоимость нитры
}

# частота спавна
var line_freq: float = 10.0
var loot_freq: float = 30.0
# таймеры
var line_timer: float = loot_freq / speed
var loot_timer: float = line_freq / speed

var is_game_paused: bool = true

var is_music_on: bool = true


# - - - - BUILTINS - - - -


func _ready() -> void:
	var _rand = rand_seed(1)
	set_car_position(LEFT)


func _process(delta: float) -> void:
	if not is_game_paused:
		loot_timer -= delta
		line_timer -= delta
		# road lines
		if line_timer <= 0:
			line_timer = line_freq / speed
			var container_height = MyLines.get_rect().size.y
			var line = Line.instance()
			line.speed = speed
			line.container_height = container_height
			MyLines.add_child(line)
		# loot
		if loot_timer <= 0:
			loot_timer = loot_freq / speed

			var item = Item.instance()
			var container_size = MyItemSpawn.get_rect().size
			item.speed = speed
			item.container_size = container_size
			item.car_dir = car_dir
			item.connect("item_obtained", self, "_on_Item_item_obtained")
			# направление
			if rand_range(0, 1) < 0.5:
				item.dir = RIGHT
			else:
				item.dir = LEFT
			# тип
			if rand_range(0, 1) >= 0.88:
				item.type = NITRO
				item.texture = textures[NITRO]
			else:
				item.type = TURBO
				item.texture = textures[TURBO]
			MyItemSpawn.add_child(item)


# - - - - METHODS - - - -


func set_car_position(side: int) -> void:
	match side:
		LEFT:
			if car_dir != LEFT:
				var _i = MyTween.interpolate_property(MyCar, "rect_position:x", car_pos[1], car_pos[0], 0.1)
				car_dir = LEFT
			else:
				return
		RIGHT:
			if car_dir != RIGHT:
				var _i = MyTween.interpolate_property(MyCar, "rect_position:x", car_pos[0], car_pos[1], 0.1)
				car_dir = RIGHT
			else:
				return
	for i in get_tree().get_nodes_in_group("item_group"):
		i.car_dir = car_dir
	if not MyTween.is_active():
		var _s = MyTween.start()


func change_speed(nitro: bool) -> void:
	if nitro:
		speed *= nitro_up
		nitro_state = true
		MyCar.get_node("TextureRect").rect_position.x = -MyCar.get_rect().size.x
	else:
		speed /= nitro_up
		nitro_state = false
		MyCar.get_node("TextureRect").rect_position.x = 0
	for i in get_tree().get_nodes_in_group("speed_group"):
		i.speed = speed


func pause_game(pause: bool) -> void:
	if pause:
		speed = 0
		is_game_paused = true
	else:
		is_game_paused = false
		if nitro_state:
			speed = 50.0 * nitro_up
		else:
			speed = 50.0
	for i in get_tree().get_nodes_in_group("speed_group"):
		i.speed = speed


func reset_game() -> void:
	speed = 50.0
	nitro_state = false
	line_timer = loot_freq / speed
	loot_timer = line_freq / speed


# - - - - SIGNALS - - - -


func _on_Item_item_obtained(type: int) -> void:
	match type:
		TURBO:
			if is_music_on:
				AudioTurbo.play()
			emit_signal("increase_score", type, cost.turbo)
		NITRO:
			if is_music_on:
				AudioNitro.play()
			emit_signal("increase_score", type, cost.nitro)
