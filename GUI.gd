extends Control


signal pause_button_pressed
signal left_button_pressed
signal right_button_pressed
signal nitro_button_pressed
signal nitro_wasted


onready var NitroScore: Label = get_node("Nitro/Label")
onready var TurboScore: Label = get_node("Turbo/Label")
onready var MyTween: Tween = get_node("Tween")
onready var AudioNitroOn: AudioStreamPlayer = get_node("AudioNitroOn")


enum { NITRO, TURBO } # тип айтема

var turbo: int = 0
var nitro: float = 0.0
var nitro_max: int = 1000.0
var nitro_cut = 10 # использование нитры
var is_nitro_active: bool = false

var nitro_timer: float = 0.25

var is_music_on: bool = true


# - - - - BUILTINS - - - -


func _process(delta: float) -> void:
	if is_nitro_active:
		nitro_timer -= delta
		if nitro_timer <= 0:
			if nitro - nitro_cut >= 0:
				var _i = MyTween.interpolate_property(self, "nitro", nitro, nitro - nitro_cut, 0.15)
				var _s = MyTween.start()
				nitro_timer = 0.25
			else:
				nitro = 0
				is_nitro_active = not is_nitro_active
				emit_signal("nitro_wasted")
	TurboScore.text = "%s" % round(turbo)
	NitroScore.text = "%s/%s" % [round(nitro), nitro_max]




# - - - - METHODS - - - -


func increase_score(type, score) -> void:
	match type:
		NITRO:
			var nitro_add = nitro + score
			if nitro_add > nitro_max:
				nitro_add = nitro_max
			var _i = MyTween.interpolate_property(self, "nitro", nitro, nitro_add, 0.2)
		TURBO:
			var _i = MyTween.interpolate_property(self, "turbo", turbo, turbo + score, 0.2)
	var _s = MyTween.start()


func reset_score() -> void:
	turbo = 0
	nitro = 0
	is_nitro_active = false
	nitro_timer = 0.25


# - - - - SIGNALS - - - -


func _on_BtnPause_pressed() -> void:
	emit_signal("pause_button_pressed")


func _on_BtnLeft_pressed() -> void:
	emit_signal("left_button_pressed")


func _on_BtnRight_pressed() -> void:
	emit_signal("right_button_pressed")


func _on_BtnNitro_pressed() -> void:
	if not is_nitro_active and nitro > 0:
		emit_signal("nitro_button_pressed")
		is_nitro_active = not is_nitro_active
		if is_music_on:
			AudioNitroOn.play()
