extends TextureRect


var speed:float
var speed_encrease:float
var pos:Vector2
var container_height: float


func _ready() -> void:
	pos = self.get_rect().position
	speed_encrease = speed / 10


func _process(delta: float) -> void:
	pos.y += speed * delta
	speed += speed_encrease
	self.set_position(pos)
	if pos.y > container_height:
		queue_free()
