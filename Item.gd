extends TextureRect


signal item_obtained


enum { LEFT, RIGHT } # направление
enum { NITRO, TURBO } # тип айтема

var speed:float
var speed_encrease:float
var car_dir: int

var pos:Vector2
var container_size: Vector2
var dir: int
var dir_offset: PoolRealArray = PoolRealArray([ 40.0, 140.0 ])
var item_size: PoolRealArray = PoolRealArray([ 1.0, 1.4 ])
var type: int
var pos_x_start: float


func _ready() -> void:
	var rect:Rect2 = self.get_rect()
	# origin
	self.rect_pivot_offset = Vector2(rect.size.x / 2, rect.size.y)
	# отражение айтема
	$TextureRect.texture = self.texture
	$TextureRect.set_position(Vector2(0.0, rect.size.y - 5.0))
	# айтем
	pos = rect.position
	pos_x_start = pos.x
	pos.y -= rect.size.y
	speed_encrease = speed / 10
	var center: float = container_size.x / 2
	if dir == LEFT:
		pos_x_start = center - dir_offset[0] - rect.size.x / 2
	elif dir == RIGHT:
		pos_x_start = center + dir_offset[0] - rect.size.x / 2
	pos.x = pos_x_start
	self.set_position(pos)
	self.rect_scale = get_item_size()


func _process(delta: float) -> void:
	var pos_x: float = get_offset()
	pos.y += speed * delta
	if dir == LEFT:
		pos.x = pos_x_start - pos_x
	else:
		pos.x = pos_x_start + pos_x
	speed += speed_encrease

	self.set_position(pos)
	self.rect_scale = get_item_size()
	# obtained
	if pos.y >= container_size.y / 2 and dir == car_dir:
		emit_signal("item_obtained", type)
		queue_free()
	# died
	if pos.y > container_size.y:
		queue_free()


func get_offset() -> float:
	var rect = self.get_rect()
	return lerp(dir_offset[0], dir_offset[1], (rect.position.y) / container_size.y)


func get_item_size() -> Vector2:
	var rect = self.get_rect()
	var size = lerp(item_size[0], item_size[1], (rect.position.y) / (container_size.y - 70.0))
	return Vector2(size, size)
