extends Node


onready var MyStart:Control = get_node("Start")
onready var MyInGameMenu:Control = get_node("InGameMenu")
onready var MyGUI:Control = get_node("GUI")
onready var MyGame:Control = get_node("Game")
onready var AudioBg:AudioStreamPlayer = get_node("AudioBg")


enum { LEFT, RIGHT } # направление

var is_music_on = true setget set_music_status, get_music_status


# - - - - BUILTINS - - - -


func _ready() -> void:
	var _start_button_pressed = MyStart.connect("start_button_pressed", self, "_on_MyStart_start_button_pressed")
	var _music_setting_changed = MyStart.connect("music_setting_changed", self, "_on_MyStart_music_setting_changed")

	var _pause_button_pressed = MyGUI.connect("pause_button_pressed", self, "_on_MyGUI_pause_button_pressed")
	var _left_button_pressed = MyGUI.connect("left_button_pressed", self, "_on_MyGUI_left_button_pressed")
	var _right_button_pressed = MyGUI.connect("right_button_pressed", self, "_on_MyGUI_right_button_pressed")
	var _nitro_button_pressed = MyGUI.connect("nitro_button_pressed", self, "_on_MyGUI_nitro_button_pressed")
	var _nitro_wasted = MyGUI.connect("nitro_wasted", self, "_on_MyGUI_nitro_wasted")

	var _increase_score = MyGame.connect("increase_score", self, "_on_MyGame_increase_score")

	var _ingame_menu_close_button_pressed = MyInGameMenu.connect("ingame_menu_close_button_pressed", self, "_on_My_InGameMenu_ingame_menu_close_button_pressed")
	var _ingame_menu_replay_button_pressed = MyInGameMenu.connect("ingame_menu_replay_button_pressed", self, "_on_My_InGameMenu_ingame_menu_replay_button_pressed")

	AudioBg.playing = get_music_status()
	show_hide_view(1)


# - - - - METHODS - - - -


func show_hide_view(index: int) -> void:
	match index:
		1:
			MyStart.show()
			MyInGameMenu.hide()
			MyGUI.hide()
			MyGame.hide()
		2:
			MyStart.hide()
			MyInGameMenu.hide()
			MyGUI.show()
			MyGame.show()
		3:
			MyStart.hide()
			MyInGameMenu.show()
			MyGUI.hide()
			MyGame.show()


func set_music_status(music_status: bool) -> void:
	is_music_on = music_status
	MyGame.is_music_on = music_status
	MyGUI.is_music_on = music_status


func get_music_status() -> bool:
	return is_music_on


# - - - - SIGNALS - - - -


func _on_MyStart_start_button_pressed() -> void:
	show_hide_view(2)
	MyGame.pause_game(false)
	MyGame.is_game_paused = false


func _on_MyStart_music_setting_changed(music_status: bool) -> void:
	set_music_status(music_status)
	AudioBg.playing = music_status


func _on_MyGUI_pause_button_pressed() -> void:
	show_hide_view(3)
	MyGame.pause_game(true)


func _on_MyGUI_left_button_pressed() -> void:
	MyGame.set_car_position(LEFT)


func _on_MyGUI_right_button_pressed() -> void:
	MyGame.set_car_position(RIGHT)


func _on_MyGUI_nitro_button_pressed() -> void:
	MyGame.change_speed(true)


func _on_MyGUI_nitro_wasted() -> void:
	MyGame.change_speed(false)


func _on_MyGame_increase_score(type, score) -> void:
	MyGUI.increase_score(type, score)


func _on_My_InGameMenu_ingame_menu_close_button_pressed() -> void:
	show_hide_view(2)
	MyGame.pause_game(false)


func _on_My_InGameMenu_ingame_menu_replay_button_pressed() -> void:
	show_hide_view(1)
	MyGame.pause_game(true)
	MyGame.reset_game()
	MyGUI.reset_score()